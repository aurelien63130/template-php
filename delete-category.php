<?php
    require 'functions/db-connect.php';
    require 'functions/category-function.php';
    $id = $_GET['id'];

    deleteCategory($pdo, $id);

    header('Location: admin-category.php?success=delete');
?>