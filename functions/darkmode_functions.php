<?php

// Une fonction utilitaire qui retourne la valeur du darkmode.
// Si on a pas de cookie darkmode, elle utilisera le mode black
function getDarkmode(){
    $mode = 'dark';
    // Sinon elle utilisera la valeur du cookie
    if(isset($_COOKIE['darkmode'])){
        $mode = $_COOKIE['darkmode'];
    }

    // Je retourne mon mode je pourrais le stocker dans une variable
    return $mode;
}

// Cette fonction prend en paramètre un mode (black ou white)
function setDarkMode($dark){
    // Elle cré un nouveau cookie avec la clé darkmode et la valeur passé en paramètre
    // Ce cookie sera valide pendant 50 seconde
    setcookie("darkmode", $dark, time() + (3600*72));
}