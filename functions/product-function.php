<?php
    function getAllProducts(){
        return [
            [
                'id'=> 1,
                'nom'=> 'Croquette pour chat',
                'price'=>100
            ],
            [
                'id'=> 2,
                'nom'=> 'Paniere pour chien',
                'price'=>200
            ],
            [
                'id'=> 3,
                'nom'=> 'Harnais',
                'price'=>150
            ]
        ];
    }

    function displayProduct($product){
    /*Ici, j'ai ajouté un formulaire avec des inputs hidden afin d'envoyer via la mèthode
      post les différents informations à ajouté au panier. Ces informations sont
      envoyés à la page add-panier.php
    */

        echo('<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">'.$product['nom'].'</h5>
    <h6 class="card-subtitle mb-2 text-muted">'.$product['price'].' euros</h6>
    <a href="produit.php?id='.$product['id'].'" class="card-link">Fiche détaillée</a>
    <form action="add-pannier.php" method="post">
        <input name="product_name" value="'.$product['nom'].'" hidden>
        <input name="price" value="'.$product['price'].'" hidden>
        <label>Quantité</label>
        <input type="number" name="nb_element" value="1">
        <button type="submit" class="card-link">Ajouter au panier</button>
    </form>

  </div>
</div>');
    }
?>