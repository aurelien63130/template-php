<?php
    function checkUser(){
        if(!isset($_SESSION['email'])){
            header("Location: login.php");
        }
    }

    function connectUser($user){
        $_SESSION['email'] = $user['email'];
        $_SESSION['nom'] = $user['nom'];
        $_SESSION['prenom'] = $user['prenom'];
    }

    function getUserByEmail($pdo, $email){
        $request = $pdo->prepare("SELECT * FROM utilisateurs WHERE email = :email");

        $request->execute(['email'=> $email]);

        $result = $request->fetch();

        return $result;
    }

    function registerUser($pdo, $nom, $prenom, $email, $password){
        $password = password_hash($password, PASSWORD_DEFAULT);
        $request = $pdo->prepare(
            "INSERT INTO utilisateurs (nom, prenom, email, mot_de_passe) 
                VALUE (:nom, :prenom, :mail, :password)"
        );
        $request->execute([
            'nom'=> $nom,
            'prenom'=> $prenom,
            'mail'=> $email,
            'password'=> $password
        ]);
    }
?>