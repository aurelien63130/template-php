<?php
    require 'functions/db-connect.php';
    require 'functions/category-function.php';
    $errors = [];
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        // Vérifier que le nom est rempli

        if(empty($_POST['category_name'])){
            $errors[] = 'Veuillez ajouter un nom de categorie';
        }



        // Vérifier que l'ordre d'affichage est rempli

        if(empty($_POST['ordre_affichage'])){
            $errors[] = 'Veuillez renseigner l\'ordre d\'affichage';
        }


        // Verifier que ce nom n'existe pas déjà
        $categorieName = findCategoryByName($pdo, $_POST['category_name']);

        if($categorieName){
            $errors[] = 'Cette catégorie existe déjà';
        }


        // Verifier que ce nom n'existe pas déjà
        $categorieOrdreAffichage = findCategoryByDisplayOrder($pdo, $_POST['ordre_affichage']);

        if($categorieOrdreAffichage){
            $errors[] = 'Cet ordre d\'affichage n\'est pas dispo';
        }

        // Si on a pas d'erreur on enregistre la catégorie
        if(count($errors) == 0){
            addCategory($pdo, $_POST['category_name'], $_POST['ordre_affichage']);
            header('Location: admin-category.php?success=add');
        }
        // On redirige l'utilisateur vers la page de listing en ajoutant un paramètre de succès

    }
?>
<html>
<head>
    <title>Mon super business case</title>

    <?php
    // Ici sera inclu tous nos fichiers css on va se positionner dedans pour gérer le darkmode
    include "parts/global-css.php";
    include "functions/user-function.php";
    checkUser();
    ?>
</head>
<body <?php
$mode = getDarkmode();
?>
<div class="container">
    <?php
    include "parts/header.php";
    $categories = getAllCategory($pdo);
    ?>

    <h2>Ajouter une catégorie</h2>

    <a href="admin-category.php">retour</a>

    <form method="post">
        <div class="mb-3 row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Nom de la catégorie</label>
            <div class="col-sm-10">
                <input type="text" name="category_name" class="form-control" value="<?php if(isset($_POST['category_name']))echo($_POST['category_name']) ?>" id="staticEmail">
            </div>
        </div>

        <div class="mb-3 row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Ordre d'affichage</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" value="<?php if(isset($_POST['ordre_affichage']))echo($_POST['ordre_affichage']) ?>"  name="ordre_affichage" id="ordre_affichage">
            </div>
        </div>

        <input type="submit" value="Valider">
    </form>

    <?php if(count($errors)>0){
        ?>
    <h3 class="text-danger">Les erreurs du formulaire</h3>

    <ol>
        <?php
            foreach ($errors as $error){
                echo('<li>'.$error.'</li>');
            }
        ?>
    </ol>

    <?php
    }
    ?>
    <?php
    include "parts/global-scripts.php";
    ?>

</div>
</body>
</html>