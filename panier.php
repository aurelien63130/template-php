<html>
<head>
    <title>Mon super business case</title>

    <?php
    include "parts/global-css.php";
    include "functions/user-function.php";
    checkUser();
    ?>
</head>
<body>
<div class="container">
    <?php
    include "parts/header.php";
    ?>
    <h1>Mon panier</h1>
    <div class="row">
        <?php
        // J'initialise un panier vide
        $panier = [];

        // Si j'ai un panier en session je le réccupére
        if(array_key_exists('panier', $_SESSION)){
            $panier = $_SESSION['panier'];
        }

        // Si mon panier est vide j'affiche seulement un message qui indique qu'il est vide
        if(count($panier) == 0){
          echo('<h2>Aucun article dans le panier</h2>');
          // Sinon j'affiche un tableau avec tous mes produits
        } else {
        ?>
        <table>
            <thead>
                <td>Nom du produit</td>
                <td>Quantité</td>
                <td>Prix unitaire</td>
                <td>Prix total</td>
                <td>Action</td>
            </thead>
            <tbody>


        <?php
        // Pour chacun des éléments de mon panier, je vais afficher une nouvelle ligne
        // avec les infos de mon tableau
        foreach ($panier as $elementPanier){
           echo('<tr>
            <td>'.$elementPanier['product'].'</td>
            <td>'.$elementPanier['quantite'].'</td>
             <td>'.$elementPanier['price'].'</td>
             <td>'.$elementPanier['price'] * $elementPanier['quantite'].'</td>
             <td>
            <!-- Ici j\'envoie en paramètre GET dans l\'url le produit à supprimer -->
             <a href="remove-panier.php?product='.$elementPanier['product'].'">
             Supprimer du panier
             </a>
             </td>
</tr>');
        }
        ?>
            </tbody>

        </table>
        <?php
        }
        ?>
    </div>

    <?php
    include "parts/footer.php";
    ?>
</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>