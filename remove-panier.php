<?php
    // Ici vous devez METTRE session_start()

    // Je déclare une variable qui sera la clé de mon tableau de produit à supprimer
    $keyToDelete = null;

    // Je vérifie que j'ai bien un panier en session
    if(array_key_exists('panier', $_SESSION)){
        // Je parcours mon panier à la recherche de l'élément à supprimer
        foreach ($_SESSION['panier'] as $key=>$value){
            // Une fois que j'ai trouvé l'élément à supprimer
            if($value['product'] === $_GET['product']){
                // Je stocke la clé à supprimer dans une variable
                $keyToDelete = $key;
                // Je l'ai trouvé pas la peine de batailler on sort du foreach
                break;
            }
        }
    }

    // J'appelle la fonction unset pour supprimer l'élément indésirable
    unset($_SESSION['panier'][$keyToDelete]);

    // L'élément est supprimer, je renvoie l'utilisateur vers son panier
    header("Location: panier.php");
?>