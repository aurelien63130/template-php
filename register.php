<?php
    require "functions/db-connect.php";
    require "functions/user-function.php";

    $errors = [];
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // On va vérifier que tous les champs sont bien remplis
        if(empty($_POST['nom'])) {
            $errors[] = 'Veuillez saisir un nom';
        }

        if(empty($_POST['prenom'])) {
            $errors[] = 'Veuillez saisir un prénom';
        }

        if(empty($_POST['email'])) {
            $errors[] = 'Veuillez saisir un email';
        }

        if(empty($_POST['password'])) {
            $errors[] = 'Veuillez saisir un password';
        }

        if(empty($_POST['confirm_password'])) {
            $errors[] = 'Veuillez confirmer votre password';
        }

        // On vérifie que l'email est bien valide

        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $errors[] = "Ce mail n'est pas valide";
        }

    // On va vérifier qu'on a pas déjà cet email en base
        $utilisateursWithThisMail = getUserByEmail($pdo, $_POST['email']);

        if($utilisateursWithThisMail){
            $errors[] = 'Un compte avec ce mail existe déjà';
        }

        if($_POST['password'] != $_POST['confirm_password']){
            $errors[] = 'Les mots de passes ne sont pas identiques';
        }

        if(count($errors) == 0){
            registerUser($pdo, $_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['password']);
            header("Location: login.php");
        }
    }
?>
<html>
<head>
    <title>Mon super business case</title>

    <link rel="stylesheet" href="public/css/login.css">
    <?php
    include "parts/global-css.php";
    ?>
</head>
<body>

<div class="container login-container">
    <div class="row">
        <div class="col-md-6 login-form-1">
            <!-- TODO : Ajoutez une jolie image ! -->
        </div>
        <div class="col-md-6 login-form-2">
            <h3>Login for Form 2</h3>
            <form method="post">

                <div class="form-group">
                    <input type="text" name="nom" class="form-control" placeholder="Nom" value="" />
                </div>

                <div class="form-group">
                    <input type="text" name="prenom" class="form-control" placeholder="Prénom" value="" />
                </div>

                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="Your Email *" value="" />
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Your Password *" value="" />
                </div>

                <div class="form-group">
                    <input type="password" name="confirm_password" class="form-control" placeholder="Confirmez votre mot de passe" value="" />
                </div>


                <div class="form-group">
                    <input type="submit" class="btnSubmit" value="M'enreigistrer" />
                </div>
                <div class="form-group">
                    <a href="index.php" class="ForgetPwd">Revenir sur la homepage</a> <br>
                    <a href="login.php">Me connecter</a>
                </div>
            </form>

            <?php if(count($errors)>0){
                ?>
                <h3 class="text-danger">Les erreurs du formulaire</h3>

                <ol>
                    <?php
                    foreach ($errors as $error){
                        echo('<li>'.$error.'</li>');
                    }
                    ?>
                </ol>

                <?php
            }
            ?>
        </div>
    </div>
</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>