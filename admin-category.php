<html>
<head>
    <title>Mon super business case</title>

    <?php
    // Ici sera inclu tous nos fichiers css on va se positionner dedans pour gérer le darkmode
    include "parts/global-css.php";
    include "functions/user-function.php";
    checkUser();
    ?>
</head>
<body <?php
require 'functions/db-connect.php';
require 'functions/category-function.php';

$mode = getDarkmode();

?>
<div class="container">
    <?php
    include "parts/header.php";
    $categories = getAllCategory($pdo);
    ?>
<h2>Admin des catégories</h2>

    <a href="add-category.php">Ajouter une catégorie</a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Ordre d'affichage</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php
        foreach ($categories as $category){
            echo(' <tr>
            <th scope="row">'.$category['id'].'</th>
            <td>'.$category['nom'].'</td>
            <td>'.$category['ordre_affichage'].'</td>
            <td>
            <a href="delete-category.php?id='.$category['id'].'">
            <i class="fa fa-trash" title="Supprimer"></i>
            </a>
            |
            <a href="edit-category.php?id='.$category['id'].'">
            <i class="fa fa-pencil" title="Editer"></i>
            </a>
            </td>
        </tr>');
        }?>


        </tbody>
    </table>
    <?php
        if(isset($_GET['success']) && $_GET['success'] == 'add'){
            echo('    <div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <strong class="me-auto">Félicitations</strong>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            Vous venez d\'ajouter une category
            </div>
    </div>');
        }

    if(isset($_GET['success']) && $_GET['success'] == 'delete') {
        echo('    <div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <strong class="me-auto">Félicitations</strong>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            Vous venez de supprimer une category
            </div>
    </div>');
    }

    if(isset($_GET['success']) && $_GET['success'] == 'update'){
        echo('    <div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <strong class="me-auto">Félicitations</strong>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            Vous venez de mettre à jour une categorie !
            </div>
    </div>');
    }
    ?>


<?php
include "parts/global-scripts.php";
?>

</div>
</body>
</html>