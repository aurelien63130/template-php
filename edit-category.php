<?php
    $errors = [];
    require 'functions/db-connect.php';
    require 'functions/category-function.php';

    $id = $_GET['id'];

    $categoryToUpdate = getOneCategory($pdo, $id);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){

        // Le nom de la category est bien remplis
        if(empty($_POST['category_name'])){
            $errors[] = 'Veuillez renseigner un nom';
        }


        // L'ordre d'affichage est bien remplit
        if(empty($_POST['ordre_affichage'])){
            $errors[] = 'Veuillez renseigner l\'ordre d\'affichage';
        }

        // Il y a déjà ce nom dans les categ
        $categWithSameName = findOtherCategoriesWithSameName($pdo, $_POST['category_name'], $id);

        if($categWithSameName){
            $errors[] = 'Ce nom est déjà pris !';
        }
        // L'ordre d'affichage n'est pas déjà pris

         $categWithSameOrder = findOtherCategoriesWithSameOrder($pdo, $_POST['ordre_affichage'], $id);


        if($categWithSameOrder){
            $errors[] = 'Cet ordre est déjà pris !';
        }

        if(count($errors) == 0){
            updateCategory($pdo,$id, $_POST['category_name'], $_POST['ordre_affichage']);

            header('Location: admin-category.php?success=update');
        }

    }
?>

<html>
<head>
    <title>Mon super business case</title>

    <?php
    // Ici sera inclu tous nos fichiers css on va se positionner dedans pour gérer le darkmode
    include "parts/global-css.php";
    include "functions/user-function.php";
    checkUser();
    ?>
</head>
<body <?php
$mode = getDarkmode();
?>
<div class="container">
    <?php
    include "parts/header.php";
    ?>

    <h2>Modifier la catégorie <?php echo($categoryToUpdate['nom']);?></h2>

    <a href="admin-category.php">retour</a>

    <form method="post">
        <div class="mb-3 row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Nom de la catégorie</label>
            <div class="col-sm-10">
                <input type="text" value="<?php echo($categoryToUpdate['nom']) ?>" name="category_name" class="form-control" id="staticEmail">
            </div>
        </div>

        <div class="mb-3 row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Ordre d'affichage</label>
            <div class="col-sm-10">
                <input type="number" value="<?php echo($categoryToUpdate['ordre_affichage']) ?>" class="form-control"  name="ordre_affichage" id="ordre_affichage">
            </div>
        </div>

        <input type="submit" value="Valider">
    </form>

    <?php if(count($errors)>0){
        ?>
        <h3 class="text-danger">Les erreurs du formulaire</h3>

        <ol>
            <?php
            foreach ($errors as $error){
                echo('<li>'.$error.'</li>');
            }
            ?>
        </ol>

        <?php
    }
    ?>
    <?php
    include "parts/global-scripts.php";
    ?>

</div>
</body>
</html>

