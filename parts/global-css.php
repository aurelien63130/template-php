<link rel="stylesheet" href="public/css/bootstrap.min.css">
<link rel="stylesheet" href="public/css/style.css">
<script src="https://kit.fontawesome.com/6170e26e69.js" crossorigin="anonymous"></script>
<?php
// Inclusion du fichier de fonction darkmode
include 'functions/darkmode_functions.php';

// Je réccupére la valeur de mon darkmode dans une fonction
$mode = getDarkmode();

// J'affiche le lien vers le bon css en fonction de la valeur du darkmode
if($mode === 'dark'){
    echo('<link rel="stylesheet" href="public/css/dark.css">');
} else {
    echo('<link rel="stylesheet" href="public/css/white.css">');
}
?>
