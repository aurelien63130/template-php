<?php
    // Ici le fichier de connexion à ma BDD. Il défini une variable PDO
    // Si on arrive à se connecter à la BDD MySQL. Sinon il affiche une erreur (try/catch)
    require_once 'functions/db-connect.php';

    // Fonctions liés à ma table catégorie
    require_once "functions/category-function.php";

    // Fonction de mon menu
    require_once "functions/menu-function.php";

    // Je réccupére toutes mes catégories avec la fonction getAllCategory.
    // Cette fonction aura besoin de faire une requête sur ma BDD donc je lui
    // passe en paramètre ma BDD qui est définie dans le fichier de fonction db-connect
    $categoriesDatabase = getAllCategory($pdo);

    // Je cré un menu vide de base j'irais le remplir en fonction de ce que j'ai dans ma BDD
    $categiesMenu = [];

    // Je parcours les lignes de ma bdd
    foreach ($categoriesDatabase as $category) {
        // Pour chacune de mes lignes j'ajoute un élément dans mon menu
        $categiesMenu[] = [
                'nom'=> $category['nom'],
                'url'=> 'category.php?id='.$category['id']
        ];
    }

// Ici ce sont mes liens statiques qui ne proviennent pas de ma BDD
$categiesMenu[] = [
                'nom'=> 'Me déconecter',
                'url'=> 'logout.php'
        ];
$categiesMenu[] = [
                'nom'=> 'Dark mode',
                'url'=> 'darkmode.php?mode=dark'
        ];
$categiesMenu[] = [
            'nom'=> 'White mode',
            'url'=> 'darkmode.php?mode=white'
        ];

$categiesMenu[] = [
    'nom'=> 'Administration des categories',
    'url'=> 'admin-category.php'
];

    // Pour le darkmode nous avons ajouté 2 liens à notre menu. Un pour rendre le thème noir avec le paramètre get mode=dark
    // Un pour mettre en mode white avec  le paramétre get mode = white
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <?php
                displayMenu($categiesMenu);
            ?>
            <li>
                <?php
                    $nbElement = 0;
                    // Ici j'affiche le nombre d'articles complet (nbElement)
/*                    if(isset($_SESSION['panier'])){
                        foreach ($_SESSION['panier'] as $elem){
                            $nbElement+=$elem['quantite'];
                        }
                    }*/
                    // Ici j'affiche le nombre de produit sans prendre en compte la quantité avec un ternaire
                    $nbElement = isset($_SESSION['panier'])?count($_SESSION['panier']):0;


                    echo('<a href="panier.php" class="nav-link">Voir mon panier ('.$nbElement.')</a>')
                ?>

            </li>
        </ul>
    </div>
</nav>