<html>
<head>
    <title>Page category</title>

    <?php
    include "parts/global-css.php";
    include "functions/user-function.php";
    checkUser();
    ?>
</head>
<body>
<div class="container">
    <?php
    include "parts/header.php";
    require_once 'functions/db-connect.php';
    require_once 'functions/category-function.php';
    require_once 'functions/article-function.php';

    $category = getOneCategory($pdo, $_GET['id']);

    if(!$category){
        echo('<h2>Cette categ n\'existe pas </h2>');
    } else {
        echo('<h1>Bienvenue dans l\'espace '. $category['nom'].'</h1>');
    }

    $products = getArticleByCategory($pdo, $_GET['id']);

    if(count($products) == 0){
        echo('<h1 class="text-danger">Aucun article</h1>');
    }

    echo('<div class="row">');
    foreach ($products as $product){
        echo('<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="public/images/'.$product['photo'].'">
  <div class="card-body">
    <h5 class="card-title">'.$product['name'].'</h5>
    <p class="card-text"></p>
    <a href="#" class="btn btn-primary">Go somewhere</a>
  </div>
</div>');
    }
    echo('</div>');

    include "parts/footer.php";
    ?>

</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>